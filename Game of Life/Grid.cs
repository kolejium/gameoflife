﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public class Grid
    {
        private bool[,] gen;

        private RecordMaster recordMaster;

        public int Width { get; private set; }
        public int Height { get; private set; }

        public bool[,] Gen
        {
            get
            {
                return gen;
            }
            set
            {
                gen = value;
                recordMaster?.Add(value);
            }
        }

        public event EventHandler Drawed;

        private int count;
        private int seed;

        public bool this[int i, int j]
        {
            get { return gen[i, j]; }
            private set { gen[i, j] = value; }
        }

        public Grid(bool[,] map, RecordMaster master = null)
        {
            if (master != null)
                recordMaster = master;
            Gen = map;
            Width = map.GetLength(0);
            Height = map.GetLength(1);

        }

        //public Grid(int w, int h, int count, int seed, RecordMaster recordMaster)
        //{
        //    if (w < 0 || h < 0)
        //        throw new ArgumentOutOfRangeException("Width and Heigh must be positive");
        //    if (count > w * h)
        //        throw new ArgumentOutOfRangeException("The number of living cannot be greater than the number of cells");
        //    width = w;
        //    height = h;
        //    this.count = count;
        //    this.seed = seed;
        //    this.recordMaster = recordMaster;
        //}

        public async void Start()
        {
            await Generate(count, seed);
        }

        public void Draw()
        {
            Console.Clear();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (this[i, j])
                    {
                        sb.Append("x");
                    }
                    else
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("\n\r");
            };
            Console.WriteLine(sb);
            Drawed?.Invoke(null, null);
        }

        async Task Generate(int count, int seed)
        {
            //await FillGen();
            Console.WriteLine("First generation end");
            Thread.Sleep(1000);
            recordMaster.Add(gen);
            Draw();
        }

        //Task FillGen()
        //{
        //    gen = new bool[width, height];
        //    return Task.Run(async () =>
        //    {
        //        int taken = 0;
        //        Console.Clear();
        //        Random random = new Random(seed * (taken + 1));
        //        for (int i = 0; i < width; i++)
        //        {
        //            for (int j = 0; j < height; j++)
        //            {
        //                if (this[i, j] == false && Convert.ToBoolean(random.Next(2)) && taken != count)
        //                {
        //                    Toggle(i, j);
        //                    taken++;
        //                    await Task.Run(() =>
        //                    {
        //                        Console.Beep();
        //                        Console.Title = "Generation";
        //                        Console.ForegroundColor = ConsoleColor.Green;
        //                        int x = (int)((100.0 / count) * taken);
        //                        string pct = string.Format("{0,-30} {1,3}%", new string((char)0x2592, x * 30 / 100), x);
        //                        Console.CursorLeft = 0;
        //                        Console.Write(pct);
        //                    });
        //                    Console.ResetColor();
        //                }
        //            }
        //        }
                
        //    });
        //}        

        public bool IsIndex(int x, int y)
        {
            return x < Width && x >= 0 && y < Height && y >= 0;
        }

        public bool Toggle(int x, int y)
        {
            return this[x, y] = !this[x, y];
        }
    }
}
