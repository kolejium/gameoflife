﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public interface IAnimate : IProgress
    { 
        Task Save(int delay, string pattern);
        IProgress GetProgress();
    }
}
