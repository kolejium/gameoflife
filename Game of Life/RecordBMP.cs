﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public class RecordBMP : IRecord
    {
        public string Name { get; private set; }
        public string Path { get; private set; }

        public RecordBMP(string name, string path)
        {
            Name = name + ".bmp";
            Path = path + @"\" + Name;
        }

        public async Task Save(bool[,] gen)
        {
            await Task.Run(async () =>
            {
                Bitmap bitmap = await ImageHelper.GetBitmap(gen);
                bitmap.Save(Path, ImageFormat.Bmp);
                //new Bitmap(gen.GetLength(0) * 10 + 1, gen.GetLength(1) * 10 + 1);
                //for (int i = 0; i < gen.GetLength(0); i++)
                //{
                //    for (int j = 0; j < gen.GetLength(1); j++)
                //    {
                //        if (!gen[i, j])
                //        {
                //            for (int x = 0; x < 10; x++)
                //            {
                //                for (int y = 0; y < 10; y++)
                //                {
                //                    bitmap.SetPixel(i * 10 + x, j * 10 + y, Color.White);
                //                }
                //            }
                //        }
                //    }
                //}
                //bitmap.Save(Path, ImageFormat.Bmp);
            });
        }
    }
}
