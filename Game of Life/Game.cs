﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public class Game
    {
        private Grid grid;
        public Grid Grid => grid;

        private RecordMaster recordMaster;
        public RecordMaster RecordMaster => recordMaster;

        private List<Task> RecordMasterTasks;

        private Task<bool[,]> GenTask;
        CancellationTokenSource cancellationTokenSource;

        private int delay;

        public bool Stop { get; set; }

        public Game(int delay, bool record)
        {
            if (record)
            {
                recordMaster = new RecordMaster();
                RecordMasterTasks = new List<Task>();
            }            
            this.delay = delay;
        }

        public async Task Start(int width, int height, int count, int seed)
        {
            await StartGrid(width, height, count, seed, recordMaster);
            RecordMasterTasks?.Add(recordMaster?.Save());
            Task first = Task.Factory.StartNew(() => Parallel.Invoke(async () => 
            {
                if (RecordMasterTasks != null)
                    await RecordMasterTasks?[RecordMasterTasks.Count - 1];
            }, () => Grid.Draw()));
            first.Wait();
            Console.WriteLine("Input anything for resume. For break input ENTER");
            Console.ReadLine();
            cancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = cancellationTokenSource.Token;
            Parallel.Invoke(
                async () => await Run(token), 
                async () => await Task.Run(() => Input())
               );
        }

        private Action Input()
        {
            string s = Console.ReadLine();
            if (s == "1")
                delay += (delay / 10 + 1);
            if (s == "2" && delay > (delay / 10 +1))
                delay -= (delay / 10 + 1);
            if (s == "c")
            {
                cancellationTokenSource.Cancel();
                return null;
            }
            return Input();
        }

        public async Task StartGrid(int width, int height, int count, int seed, RecordMaster master)
        {
            var task = Generation(width, height, count, seed);
            grid = new Grid(await task, master);
            //task.Wait(); // Нужно! Чтобы дождаться task
            grid.Drawed += Grid_Drawed;
        }

        public async Task Run(CancellationToken token)
        {
            Grid.Gen = await NextGeneration();
            RecordMasterTasks?.Add(recordMaster?.Save());
            Parallel.Invoke(
                async () =>
                {
                    if (RecordMasterTasks != null)
                        await Task.Run(() => RecordMasterTasks[RecordMasterTasks.Count - 1]);
                }, 
                () => Grid.Draw());
            if (token.IsCancellationRequested)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Breaked!");
                if (RecordMasterTasks != null)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Wait saving! Saved: {RecordMaster.Saved}, All: {RecordMasterTasks.Count}");
                    Task.WaitAll(RecordMasterTasks.ToArray());
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Save images ended\nWould you like to make an animation based on the received pictures? (Y/N)");
                    if (Console.ReadLine() == "Y")
                    {
                        Task t = Task.Run(async () => await recordMaster.SaveAnimation(100));
                        t.Wait();
                        Console.WriteLine("Save animation end");
                    }
                }
                return;
            }
            await Run(token);
        }

        private async void Grid_Drawed(object sender, EventArgs e)
        {
            await Task.Delay(delay);
            if (GetCountAlive() == 0)
            {
                Console.WriteLine("This last grid - this end");
                cancellationTokenSource.Cancel();
            }
        }

        public async Task<bool[,]> Generation(int width, int height, int count, int seed)
        {
            Console.Title = "Generation";
            Console.ForegroundColor = ConsoleColor.Green;
            bool[,] gen = new bool[width, height];
            int taken = 0;
            while (taken != count)
            {
                Random random = new Random(seed + DateTime.Now.Millisecond * (taken + 1));
                int i = random.Next(width), j = random.Next(height);
                if (gen[i, j] == false)
                {
                    gen[i, j] = true;
                    taken++;
                    await Task.Run(() =>
                    {
                        int x = (int)((100.0 / count) * taken);                            
                        string pct = string.Format("{0,-30} {1,3}%", new string((char)0x2592, x * 30 / 100), x);
                        Console.CursorLeft = 0;
                        Console.Write(pct);
                    });                    
                }
            }
            Console.Beep();
            Console.ResetColor();
            //for (int i = 0; i < width && taken != count; i++)
            //{
            //    for (int j = 0; j < height && taken != count; j++)
            //    {
            //        if (gen[i, j] == false && Convert.ToBoolean(random.Next(2)))
            //        {
            //            gen[i, j] = true;
            //            taken++;
            //            await Task.Run(() =>
            //            {
            //                Console.Beep();
            //                Console.Title = "Generation";
            //                Console.ForegroundColor = ConsoleColor.Green;
            //                int x = (int)((100.0 / count) * taken);
            //                string pct = string.Format("{0,-30} {1,3}%", new string((char)0x2592, x * 30 / 100), x);
            //                Console.CursorLeft = 0;
            //                Console.Write(pct);
            //            });
            //            Console.ResetColor();
            //        }
            //    }
            //}
            return gen;
        }

        public async Task<bool[,]> NextGeneration()
        {
            bool[,] next = new bool[Grid.Width, Grid.Height];
            for (int i = 0; i < Grid.Width; i++)
            {
                for (int j = 0; j < Grid.Height; j++)
                {
                    int count = GetCountAliveNeighbors(i, j);
                    if (Grid[i, j] && (count < 2 || count > 3))
                        next[i, j] = !grid[i, j];
                    else if (!Grid[i, j] && count == 3)
                        next[i, j] = !grid[i, j];
                    else
                        next[i, j] = grid[i, j];
                }
            }
            return next;
        }

        public int GetCountAliveNeighbors(int x, int y)
        {
            int count = 0;
            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 1; j <= y + 1; j++)
                {
                    if (Grid.IsIndex(i, j) && (j != y || i != x) && Grid[i, j])
                        count++;
                }
            }
            return count;
        }

        public int GetCountAlive()
        {
            int count = 0;
            for (int i = 0; i < Grid.Width; i++)
            {
                for (int j = 0; j < Grid.Height; j++)
                {
                    if (Grid.IsIndex(i, j) && Grid[i, j])
                        count++;
                }
            }
            return count;
        }

    }
}
