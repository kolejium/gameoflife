﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game_of_Life
{
    public interface IProgress
    {
        int CountOperations { get; }
        int TakenOperations { get; }

        bool CanNotification(int prev);

        event Action<int, int> Notified;
    }
}
