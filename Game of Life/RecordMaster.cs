﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public class RecordMaster
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public int Number { get; set; }
        public int Saved { get; private set; }
        private Queue<bool[,]> queue;
        private List<Type> types;

        public RecordMaster()
        {
            Name = "Record " + DateTime.Now.ToString().Replace(':', '_');
            Path = Environment.CurrentDirectory + @"\" + Name;
            Directory.CreateDirectory(Path);
            queue = new Queue<bool[,]>();
            GetTypes();
        }

        public void Add(bool[,] gen)
        {
            queue.Enqueue(gen);
        }

        private void GetTypes()
        {
            types = new List<Type>();
            foreach (var item in System.Reflection.Assembly.Load("Game of Life").GetTypes())
            {
                if (item != null)
                {
                    foreach (Type type in item.GetInterfaces())
                    {
                        if (type.Name == "IRecord")
                            types.Add(item);
                    }
                }
            }
        }

        public async Task Save()
        {
            bool[,] gen = queue.Dequeue();
            string name = "#" + Number++;
            foreach (Type item in types)
            {
                ConstructorInfo ci = item.GetConstructor(new Type[] { typeof(string), typeof(string) });
                IRecord rec = (IRecord)ci.Invoke(new object[] { name, Path });
                await rec.Save(gen);                
            }
            Saved++;
        }

        public async Task SaveAnimation(int delay)
        {
            foreach (var item in types)
            {
                foreach (Type type in item.GetInterfaces())
                {
                    if (type.Name == "IAnimate")
                    {
                        ConstructorInfo ci = item.GetConstructor(new Type[] { typeof(string), typeof(string) });
                        IAnimate animate = (IAnimate)ci.Invoke(new object[] { "animation_" + item.Name, Path });
                        await animate.Save(delay, null);
                    }
                }
            }
        }
    }
}
