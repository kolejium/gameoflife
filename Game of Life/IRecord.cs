﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public interface IRecord
    {
        string Name { get; }
        string Path { get; }
        Task Save(bool[,] gen);
    }
}
