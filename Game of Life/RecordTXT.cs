﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public class RecordTXT : IRecord
    {
        public string Name { get; private set; }
        public string Path { get; private set; }

        public RecordTXT(string name, string path)
        {
            Name = name + ".txt";
            Path = path + @"\" + Name;
        }

        public async Task Save(bool[,] gen)
        {
            using (FileStream fs = new FileStream(Path, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    for (int i = 0; i < gen.GetLength(0); i++)
                    {
                        for (int j = 0; j < gen.GetLength(1); j++)
                        {
                            if (j != gen.GetLength(1) - 1)
                                await sw.WriteAsync(gen[i, j] ? "x" : " ");
                            else
                                await sw.WriteLineAsync(gen[i, j] ? "x": " ");
                        }
                    }
                }
            }
        }
    }
}
