﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game_of_Life
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Convert.ToString(Convert.ToInt64("0015CF30", 16), 2));
            Console.ReadKey();
            if (args.Length == 0)
            {
                int[] arr = new int[4];
                Console.WriteLine("Please input(!After variable input ENTER!) next variables:\n Count rows\n Count columns\n Count cells\n Delay between operations\n");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Recommendation:\n" +
                    " Count rows: 20," +
                    " Count columns 50," +
                    " Count cells 100 < x < 700," +
                    " Delay 400 < y < 1200");
                for (int i = 0; i < 4; i++)
                {
                    arr[i] = int.Parse(Console.ReadLine());
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Thanks!\n Would you like to record the generation? (Y/N)");
                bool record = Console.ReadLine() == "Y" ? true : false;
                if (record)
                    Console.WriteLine("Record naming by #generation in the directory: CurrentDicreactory/Record " + DateTime.Now);

                Console.WriteLine("Okay! Lets start!");
                Start(arr[0], arr[1], arr[2], arr[3], record);

            }
            else
            {
                Start(Convert.ToInt32(args[0]),
                    Convert.ToInt32(args[1]),
                    Convert.ToInt32(args[2]),
                    Convert.ToInt32(args[3]),
                    Convert.ToBoolean(args[4]));
            }
        }

        static void Start(int rows, int columns, int cells, int delay, bool record)
        {
            Game game = new Game(delay, record);
            Task t = game.Start(rows, columns, cells, new Random().Next(DateTime.Now.Millisecond));
            t.Wait();
            Console.WriteLine("Program work end");
            Console.ReadKey();
        }

        //static void Main(string[] args)
        //{
        //    Console.CursorVisible = false;

        //    Console.WriteLine("Loading... ");
        //    Print("Loading system...", 1000);
        //    Print("Всякая фигня", 500);
        //    Print("И т.д.", 2000);

        //    int pos = Console.CursorTop;
        //    Console.SetCursorPosition(11, 0);
        //    Console.Write("OK");
        //    Console.SetCursorPosition(0, pos);

        //    var rand = new Random();

        //    for (int i = 0; i <= 100; i++)
        //    {
        //        if (i < 25)
        //            Console.ForegroundColor = ConsoleColor.Red;
        //        else if (i < 50)
        //            Console.ForegroundColor = ConsoleColor.DarkRed;
        //        else if (i < 75)
        //            Console.ForegroundColor = ConsoleColor.DarkYellow;
        //        else if (i < 100)
        //            Console.ForegroundColor = ConsoleColor.Yellow;
        //        else
        //            Console.ForegroundColor = ConsoleColor.Green;

        //        string pct = string.Format("{0,-30} {1,3}%", new string((char)0x2592, i * 30 / 100), i);
        //        Console.CursorLeft = 0;
        //        Console.Write(pct);
        //        Thread.Sleep(rand.Next(0, 500));
        //    }
        //    Console.WriteLine();
        //    Console.ResetColor();
        //}

        //static void Print(string message, int delay)
        //{
        //    Thread.Sleep(delay);
        //    Console.WriteLine(message);
        //}
    }
}
