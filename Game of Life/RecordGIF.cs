﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public class RecordGIF : IRecord, IAnimate, IProgress
    {
        public string Name { get; private set; }
        public string Path { get; private set; }

        private int takenOperations;
        private int countOperations;

        public event Action<int, int> Notified;

        public int CountOperations
        {
            get => countOperations;
            private set => countOperations = value;
        }

        public int TakenOperations
        {
            get => takenOperations;
            private set
            {
                if (CanNotification(takenOperations))
                {
                    takenOperations = value;
                    Notified?.Invoke(takenOperations, countOperations);
                }
                else
                    takenOperations = value;
            }
        }

        public RecordGIF(string name, string path)
        {
            Name = name + ".gif";
            Path = path + @"\" + Name;
        }

        public bool CanNotification(int prev)
        {
            return prev != (int)((100.0 / CountOperations) * TakenOperations);
        }

        public async Task Save(bool[,] gen)
        {
            await Task.Run(async () =>
            {
                Bitmap bitmap = await ImageHelper.GetBitmap(gen);
                bitmap.Save(Path, ImageFormat.Gif);
            });
        }

        public async Task Save(int delay, string pattern)
        {
            Notified += new Action<int, int>((current, all) =>
            {
                Console.Title = "Creating animation";
                Console.ForegroundColor = ConsoleColor.Green;
                int x = (int)((100.0 / all) * current);
                string pct = string.Format("{0,-30} {1,3}%", new string((char)0x2592, x * 30 / 100), x);
                Console.CursorLeft = 0;
                Console.Write(pct);
            });
            if (pattern == null || pattern == "")
                pattern = "(.gif$)"; // !!!! NOT BMP!!! VERY LONG !!!!
            List<string> names = new List<string>();
            Regex reg = new Regex(pattern);
            foreach (var item in Directory.GetFiles(Path.Remove(Path.LastIndexOf(@"\"))))
            {
                if (reg.IsMatch(item))
                    names.Add(item);
            }
            if (names.Count == 0)
                return;
            Dictionary<string, string[]> dic = new Dictionary<string, string[]>();
            foreach (var item in names)
            {
                string s = item.Substring(item.LastIndexOf('.') + 1);
                if (!dic.Keys.Contains(s))
                    dic.Add(s, names.Where(x => x.EndsWith(s)).ToArray());
            }
            dic.Values.ToList().ForEach(x => CountOperations += x.Length);
            List<Action> actions = new List<Action>();
            Parallel.ForEach(dic.Keys, key =>
            {
                MagickImageCollection collection = new MagickImageCollection();
                Parallel.ForEach(dic[key].OrderBy(x => x), value =>
                {
                    Task.Run(() =>
                    {
                        lock (this)
                        {
                            TakenOperations++;
                        }
                    });
                    collection.Add(new MagickImage(value));
                });
                actions.Add(() =>
                {
                    QuantizeSettings settings = new QuantizeSettings();
                    settings.Colors = 2;
                    collection.Quantize(settings);
                    collection.Optimize();
                    collection.Coalesce();
                    collection.Write(Path.Insert(Path.LastIndexOf('.'), key));
                });
            });
            await Task.Factory.StartNew(() => Parallel.Invoke(actions.ToArray()));
        }

        public IProgress GetProgress()
        {
            return (IProgress)this;
        }
    }
}
