﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace Game_of_Life
{
    public static class ImageHelper
    {
        public static async Task<Bitmap> GetBitmap(bool[,] gen, int coeff = 10)
        {
            return await Task.Run<Bitmap>(() =>
            {
                Bitmap bitmap = new Bitmap(gen.GetLength(0) * coeff, gen.GetLength(1) * coeff);
                for (int i = 0; i < gen.GetLength(0); i++)
                {
                    for (int j = 0; j < gen.GetLength(1); j++)
                    {
                        if (!gen[i, j])
                        {
                            for (int x = 0; x < 10; x++)
                            {
                                for (int y = 0; y < 10; y++)
                                {
                                    bitmap.SetPixel(i * 10 + x, j * 10 + y, Color.White);
                                }
                            }
                        }
                    }
                }
                return bitmap;
            });
        }
    }
}
